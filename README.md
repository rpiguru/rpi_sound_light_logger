# Raspberry Pi data logger.

## Components

- Raspberry Pi 3
- USB Mic for sound level detection
    
    https://www.amazon.com/Kinobo-Microphone-Desktop-Recognition-Software/dp/B00IR8R7WQ
    
- Lux sensor for light level detection. (TSL2591)
    
    https://www.adafruit.com/products/1980
    
    https://www.amazon.com/Adafruit-TSL2591-Dynamic-Digital-ADA1980/dp/B00XW2OFWW/ref=sr_1_1?ie=UTF8&qid=1479873229&sr=8-1&keywords=TSL2591


## Wiring components. 

    
## Install dependencies for backend software.

### Update & Upgrade packages after expanding file system.
 
- Open the configuration GUI with `sudo raspi-config` and expand file system.

- It will ask for rebooting after applying change, and update & upgrade packages to the latest version.
    
        sudo apt-get update
        sudo apt-get upgrade
        
### Enable I2C bus on Raspberry Pi.

Enable I2C bus by following below:
    
   https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c 
    
### Install dependencies
    
- Install dependencies for audio analyze

        cd ~
        mkdir tmp
        cd tmp
        sudo apt-get install libpython-dev
        sudo apt-get install python-pyaudio
        sudo pip install pyaudio 
        sudo pip install numpy
        git clone https://github.com/ExCiteS/SLMPi.git
        cd SLMPi/SoundAnalyse-0.1.1
        sudo python setup.py install

- Install dependencies for Light Sensor

        sudo apt-get install libffi-dev
        cd ~/tmp
        git clone https://github.com/maxlklaxl/python-tsl2591.git
        cd python-tsl2591
        sudo python setup.py install
        
        
### Clone the repository into Raspberry Pi
    
    cd ~
    git clone https://rpi_guru@bitbucket.org/rpi_guru/rpi_sound_light_logger.git

## Install web sever on RPi.
    
### Install dependencies for Apache2 web server
    
    sudo apt-get install apache2 libapache2-mod-wsgi
    
###	Copy sensor_ctrl folder to `/var/www`. 

Before copying, give permission to the destination directory.
    
    sudo chmod 777 -R /var/www
    mkdir /var/www/sensor_ctrl
    cp -r /home/pi/rpi_sound_light_sensor/sensor_ctrl/* /var/www/sensor_ctrl
    
- Configure Apache2 web server
        
        cd /var/www/sensor_ctrl
        sudo pip install –r requirements.txt
        sudo chmod 777 manage.py
        sudo ./manage.py syncdb
        
- Enter the user information to create your login user.

        You just installed Django's auth system, which means you don't have any superusers defined.
        Would you like to create one now? (yes/no): yes
        Username (leave blank to use 'root'): admin (Enter your desired username)
        Email address: xxxxx@xxxxxx.xxx (Enter your email address)
        Password: xxxxx (Enter your desired password)
        Password (again): xxxxx (Enter your password again)

- Change the owner of `sensor_ctrl` directory to `www-data`
    
        cd ..
        sudo chown –R www-data.www-data /var/www/sensor_ctrl
        sudo chmod 777 -R /var/www/sensor_ctrl
        
-	Copy `sensor_ctrl.conf` to `/etc/apache2/sites-enabled`

    Before copying, give permission to the destination directory.

        sudo chmod 777 -R /etc/apache2/sites-enabled
    
    Remove old configuration file.
        
        sudo rm /etc/apache2/sites-enabled/000-default.conf
        
    Copy configuration file.
        
        sudo cp /home/pi/rpi_sound_light_logger/sensor_ctrl/sensor_ctrl.conf /etc/apache2/sites-enabled/

### Start web server

- Create log file and change ownership.

        sudo chmod 777 -R /home/pi/rpi_sound_light_logger/
        touch /home/pi/rpi_sound_light_logger/log.txt
        sudo chown www-data.www-data /home/pi/rpi_sound_light_logger/log.txt
        
- Start server
    
        sudo service apache2 restart
        
### Enable auto-start of backend script.

    sudo nano /etc/rc.local

And add this before `exit 0` line:

    (sleep 10; python /home/pi/rpi_sound_light_logger/data_logger.py)&




