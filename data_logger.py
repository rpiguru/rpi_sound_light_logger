"""

"""
import logging
import os
import random
import sqlite3 as lite
import time
import xml.etree.ElementTree
from audio_analyser import AudioAnalyser
import tsl2591


tsl = tsl2591.Tsl2591()  # initialize
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'


class DataLogger:

    log_level = 10
    log_file_name = ''
    db_name = ''
    rate = 10
    audio_ctrl = None
    debug = False

    def __init__(self):

        self.debug = True

        self.db_name = cur_dir + self.get_param_from_xml('DB_NAME')
        self.audio_ctrl = AudioAnalyser()

        self.update_params()
        self.check_table()

    def update_params(self):
        """
        update parameters from configuration file
        :return:
        """
        self.rate = int(self.get_param_from_xml('READING_RATE'))

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(cur_dir + 'config.xml').getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break
        return tmp

    def check_table(self):
        """
        Check whether data table exists or not
        If not exists, create it.
        Column : No. id, val, timestamp
        """

        conn = None
        try:
            conn = lite.connect(self.db_name)
            os.system('chmod 777 ' + self.db_name)     # change permission of db file
            curs = conn.cursor()
            sql = 'create table if not exists tb_sensor ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'sound_level NUMERIC , ' \
                  'light_level NUMERIC , ' \
                  'timestamp DATETIME' \
                  ');'
            curs.execute(sql)
            conn.commit()
            conn.close()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Creating Table failed...")
            return False
        finally:
            if conn:
                conn.close()

    def get_light_level(self):
        """
        Get light value from TSL2591 sensor
        :return:
        """
        # TODO: add logic here
        val_list = []
        if self.debug:
            for i in range(20):
                val_list.append(random.randint(0, 100) / 20.0)
        else:
            for i in range(20):
                val = read_light()
                if val is not None:
                    val_list.append(val)

        # remove max/min value from the list
        val_list.sort()
        val_list.pop(0)
        val_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
        return avg_val

    def get_sound_level(self):
        """
        Get sound level from the USB mic
        :return:
        """
        # TODO: add logic here
        val_list = []
        if self.debug:
            for i in range(20):
                val_list.append(random.randint(0, 100) / 20.0)
        else:
            for i in range(20):
                val = self.audio_ctrl.get_audio_level()
                if val is not None:
                    val_list.append(val)

        # remove max/min value from the list
        val_list.sort()
        val_list.pop(0)
        val_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
        return avg_val

    def run(self):
        """
        """
        light_val = self.get_light_level()
        sound_val = self.get_sound_level()

        return self.upload_to_table((sound_val, light_val))

    def upload_to_table(self, data):
        conn = None
        try:
            conn = lite.connect(self.db_name)
            os.system('chmod 777 ' + self.db_name)     # change permission of db file
            curs = conn.cursor()
            sql = "INSERT INTO tb_sensor (sound_level, timestamp, light_level) values(" + ("%.2f" % data[0]) + \
                  ", datetime('now', 'localtime'), " + ("%.2f" % data[1]) + ");"
            print sql
            curs.execute(sql)
            conn.commit()
            conn.close()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Failed to insert data ...")
            return False
        finally:
            if conn:
                conn.close()


def read_light():
    """
    Read lux value from TSL2591 module
    :return:
    """
    full, ir = tsl.get_full_luminosity()  # read raw values (full spectrum and ir spectrum)
    lux = tsl.calculate_lux(full, ir)  # convert raw values to lux
    return lux


if __name__ == '__main__':

    print "Initializing Sensor..."
    ctrl = DataLogger()

    # initialize logging
    log_file_name = ctrl.get_param_from_xml('LOG_FILE')
    log_level = int(ctrl.get_param_from_xml('LOG_LEVEL'))
    logging.basicConfig(level=log_level, filename=log_file_name,
                        format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    while True:
        ctrl.update_params()
        start_time = time.time()
        ctrl.run()

        while time.time() - start_time < ctrl.rate:
            time.sleep(0.2)
